@extends('layouts.master')

@section('title')
Show Cast
@endsection

@section('content')
<h4>{{$cast->nama}}</h4>
<p>(Age {{$cast->umur}})</p>
<p>{{$cast->bio}}</p>
@endsection