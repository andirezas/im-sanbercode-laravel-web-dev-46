@extends('layouts.master')

@section('title')
Tambah Data Cast
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="mb-3">
      <label>Nama Caster</label>
      <input type="text" class="form-control" name="nama" aria-describedby="emailHelp">
            @error('nama')
                <div class="alert alert-danger">
                {{ $message }}
                </div>
            @enderror
    </div>
    <div class="mb-3">
        <label>Umur</label>
        <input type="text" class="form-control" name="umur" aria-describedby="emailHelp">
            @error('umur')
            <div class="alert alert-danger">
            {{ $message }}
            </div>
            @enderror
      </div>
    <div class="mb-3">
      <label>Biografi</label>
      <textarea type="text" class="form-control" name="bio" cols="30" rows="10" ></textarea>
            @error('bio')
            <div class="alert alert-danger">
            {{ $message }}
            </div>
            @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection