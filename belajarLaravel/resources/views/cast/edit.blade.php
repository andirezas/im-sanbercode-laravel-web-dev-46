@extends('layouts.master')

@section('title')
Edit Data Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="mb-3">
      <label>Nama Caster</label>
      <input type="text" class="form-control" class="@error('nama') is-invalid @enderror" value="{{$cast->nama}}" name="nama" aria-describedby="emailHelp">
            @error('nama')
                <div class="alert alert-danger">
                {{ $message }}
                </div>
            @enderror
    </div>
    <div class="mb-3">
        <label>Umur</label>
        <input type="text" class="form-control" class="@error('umur') is-invalid @enderror" value="{{$cast->umur}}" name="umur" aria-describedby="emailHelp">
            @error('umur')
            <div class="alert alert-danger">
            {{ $message }}
            </div>
            @enderror
      </div>
    <div class="mb-3">
      <label>Biografi</label>
      <textarea type="text" class="form-control" class="@error('bio') is-invalid @enderror" name="bio" cols="30" rows="10" >{{$cast->bio}}</textarea>
            @error('bio')
            <div class="alert alert-danger">
            {{ $message }}
            </div>
            @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection