@extends('layouts.master')

@section('title')
Buat Account Baru
@endsection

@section('content')
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
    @csrf
    <label>First Name:</label><br>
    <input type="text" name="fnama"><br><br>
    <label>Last Name:</label><br>
    <input type="text" name="lnama"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" value="M" name="gender"> Male <br>
    <input type="radio" value="F" name="gender"> Female <br>
    <input type="radio" value="O" name="gender"> Other <br><br>
    <label>Nationality:</label><br><br>
    <select name="national" id="">
        <option value="1">Indonesia</option>
        <option value="2">Malaysia</option>
        <option value="3">Japan</option>
        <option value="4">Brunei</option>
        <option value="5">Singapore</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox" value="INA" name="spoken">Bahasa Indonesia <br>
    <input type="checkbox" value="ENG" name="spoken">English <br>
    <input type="checkbox" value="OTH" name="spoken">Other <br><br>
    <label>Bio:</label><br><br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
</form>
@endsection