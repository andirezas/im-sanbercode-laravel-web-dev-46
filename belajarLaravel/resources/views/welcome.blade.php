@extends('layouts.master')

@section('title')
Selamat Datang!
@endsection

@section('content')
Terima kasih telah bergabung di SanberBook {{$namaDepan}} {{$namaBelakang}}. Social Media kita bersama!
@endsection