<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function welcome(Request $request)
    {
        $namaDepan = $request-> input('fnama');
        $namaBelakang = $request-> input('lnama');
        // dd($request ['fnama']);
        return view('welcome', ['namaDepan'=> $namaDepan, 'namaBelakang'=> $namaBelakang]);
    }
}
