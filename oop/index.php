<?php
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$sheep = new Animal("shaun");

echo "Nama Hewan : " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>"; // "no"

echo "------------- <br>";

$kodok = new Frog("buduk");
echo "Nama Hewan : " . $kodok->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $kodok->legs . "<br>"; // 4
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; // "no"
$kodok->jump();

echo "------------- <br>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewan : " . $sungokong->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sungokong->legs . "<br>"; // 4
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
$sungokong->yell();
